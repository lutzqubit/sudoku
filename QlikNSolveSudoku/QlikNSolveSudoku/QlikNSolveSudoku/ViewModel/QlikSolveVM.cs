﻿// ***********************************************************************
// Assembly         : QlikNSolveSudoku
// Author           : Lutse Imobekhai
// Created          : 10-25-2016
// 
// Last Modified By : Lutse Imobekhai
// Last Modified On : 10-30-2016
// ***********************************************************************
//
// <summary>
//       This project is located at
//      https://bitbucket.org/lutzqubit/sudoku/src
//      This is the viewmodel that contains the logic and observable objects that are wired up with
//       the view
// </summary>
// ***********************************************************************
// 


using QlikNSolveSudoku.Model;
using QlikNSolveSudoku.Views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace QlikNSolveSudoku.ViewModel
{

    public class QlikSolveVM : INotifyPropertyChanged
    {
        

        public event PropertyChangedEventHandler PropertyChanged;
        
        private int[,] boardMatrix;
        public int[,] BoardMatrix { get { return boardMatrix; }
                                    set {
                                         boardMatrix = value;
                                         OnPropertyChanged("BoardMatrix");
                                         }
                                  }
        private int[,] solvedBoard;
        public int[,] SolvedBoard { get { return solvedBoard; } set { solvedBoard = value; } }
        private List<int> possibleValues;
        public List<int> PossibleValues
        {
            get { return possibleValues; }

             set
            {
                possibleValues = value;
                OnPropertyChanged("PossibleValues");
            }
        }



        private bool isLoading;
        public bool IsLoading
        {
            get { return isLoading; }
            set
            {
                isLoading = value;
                OnPropertyChanged("IsLoading");
            }
        }

    


        #region ROW 1
        public int Row00
        {
            get { return boardMatrix[0,0]; }

            set
            {
                boardMatrix[0, 0] = value;
                OnPropertyChanged("Row00");
            }
        }

        public int Row01
        {
            get { return boardMatrix[0, 1]; }

            set
            {
                boardMatrix[0, 1] = value;
                OnPropertyChanged("Row01");
            }
        }

        public int Row02
        {
            get { return boardMatrix[0, 2]; }

            set
            {
                boardMatrix[0, 2] = value;
                OnPropertyChanged("Row02");
            }
        }

        public int Row03
        {
            get { return boardMatrix[0, 3]; }

            set
            {
                boardMatrix[0, 3] = value;
                OnPropertyChanged("Row03");
            }
        }

        public int Row04
        {
            get { return boardMatrix[0, 4]; }

            set
            {
                boardMatrix[0, 4] = value;
                OnPropertyChanged("Row04");
            }
        }

        public int Row05
        {
            get { return boardMatrix[0, 5]; }

            set
            {
                boardMatrix[0, 5] = value;
                OnPropertyChanged("Row05");
            }
        }

        public int Row06
        {
            get { return boardMatrix[0, 6]; }

            set
            {
                boardMatrix[0, 6] = value;
                OnPropertyChanged("Row06");
            }
        }

        public int Row07
        {
            get { return boardMatrix[0, 7]; }

             set
            {
                boardMatrix[0, 7] = value;
                OnPropertyChanged("Row07");
            }
        }

        public int Row08
        {
            get { return boardMatrix[0, 8]; }

            set
            {
                boardMatrix[0, 8] = value;
                OnPropertyChanged("Row08");
            }
        }




        #endregion
        #region ROW 2
        public int Row10
        {
            get { return boardMatrix[1, 0]; }

            set
            {
                boardMatrix[1, 0] = value;
                OnPropertyChanged("Row10");
            }
        }

        public int Row11
        {
            get { return boardMatrix[1, 1]; }

            set
            {
                boardMatrix[1, 1] = value;
                OnPropertyChanged("Row11");
            }
        }

        public int Row12
        {
            get { return boardMatrix[1, 2]; }

             set
            {
                boardMatrix[1, 2] = value;
                OnPropertyChanged("Row12");
            }
        }

        public int Row13
        {
            get { return boardMatrix[1, 3]; }

            set
            {
                boardMatrix[1, 3] = value;
                OnPropertyChanged("Row13");
            }
        }

        public int Row14
        {
            get { return boardMatrix[1, 4]; }

            set
            {
                boardMatrix[1, 4] = value;
                OnPropertyChanged("Row14");
            }
        }

        public int Row15
        {
            get { return boardMatrix[1, 5]; }

            set
            {
                boardMatrix[1, 5] = value;
                OnPropertyChanged("Row15");
            }
        }

        public int Row16
        {
            get { return boardMatrix[1, 6]; }

             set
            {
                boardMatrix[1, 6] = value;
                OnPropertyChanged("Row16");
            }
        }

        public int Row17
        {
            get { return boardMatrix[1, 7]; }

            set
            {
                boardMatrix[1, 7] = value;
                OnPropertyChanged("Row17");
            }
        }

        public int Row18
        {
            get { return boardMatrix[1, 8]; }

            set
            {
                boardMatrix[1, 8] = value;
                OnPropertyChanged("Row18");
            }
        }




        #endregion
        #region ROW 3
        public int Row20
        {
            get { return boardMatrix[2, 0]; }

             set
            {
                boardMatrix[2, 0] = value;
                OnPropertyChanged("Row20");
            }
        }

        public int Row21
        {
            get { return boardMatrix[2, 1]; }

             set
            {
                boardMatrix[2, 1] = value;
                OnPropertyChanged("Row21");
            }
        }

        public int Row22
        {
            get { return boardMatrix[2, 2]; }

             set
            {
                boardMatrix[2, 2] = value;
                OnPropertyChanged("Row22");
            }
        }

        public int Row23
        {
            get { return boardMatrix[2, 3]; }

             set
            {
                boardMatrix[2, 3] = value;
                OnPropertyChanged("Row23");
            }
        }

        public int Row24
        {
            get { return boardMatrix[2, 4]; }

             set
            {
                boardMatrix[2, 4] = value;
                OnPropertyChanged("Row24");
            }
        }

        public int Row25
        {
            get { return boardMatrix[2, 5]; }

             set
            {
                boardMatrix[2, 5] = value;
                OnPropertyChanged("Row25");
            }
        }

        public int Row26
        {
            get { return boardMatrix[2, 6]; }

             set
            {
                boardMatrix[2, 6] = value;
                OnPropertyChanged("Row26");
            }
        }

        public int Row27
        {
            get { return boardMatrix[2, 7]; }

             set
            {
                boardMatrix[2, 7] = value;
                OnPropertyChanged("Row27");
            }
        }

        public int Row28
        {
            get { return boardMatrix[2, 8]; }

             set
            {
                boardMatrix[2, 8] = value;
                OnPropertyChanged("Row28");
            }
        }




        #endregion    
        #region ROW 4
        public int Row30
        {
            get { return boardMatrix[3, 0]; }

             set
            {
                boardMatrix[3, 0] = value;
                OnPropertyChanged("Row30");
            }
        }

        public int Row31
        {
            get { return boardMatrix[3, 1]; }

             set
            {
                boardMatrix[3, 1] = value;
                OnPropertyChanged("Row31");
            }
        }

        public int Row32
        {
            get { return boardMatrix[3, 2]; }

             set
            {
                boardMatrix[3, 2] = value;
                OnPropertyChanged("Row32");
            }
        }

        public int Row33
        {
            get { return boardMatrix[3, 3]; }

             set
            {
                boardMatrix[3, 3] = value;
                OnPropertyChanged("Row33");
            }
        }

        public int Row34
        {
            get { return boardMatrix[3, 4]; }

             set
            {
                boardMatrix[3, 4] = value;
                OnPropertyChanged("Row34");
            }
        }

        public int Row35
        {
            get { return boardMatrix[3, 5]; }

             set
            {
                boardMatrix[3, 5] = value;
                OnPropertyChanged("Row35");
            }
        }

        public int Row36
        {
            get { return boardMatrix[3, 6]; }

             set
            {
                boardMatrix[3, 6] = value;
                OnPropertyChanged("Row36");
            }
        }

        public int Row37
        {
            get { return boardMatrix[3, 7]; }

             set
            {
                boardMatrix[3, 7] = value;
                OnPropertyChanged("Row37");
            }
        }

        public int Row38
        {
            get { return boardMatrix[3, 8]; }

             set
            {
                boardMatrix[3, 8] = value;
                OnPropertyChanged("Row38");
            }
        }




        #endregion
        #region ROW 5
        public int Row40
        {
            get { return boardMatrix[4, 0]; }

             set
            {
                boardMatrix[4, 0] = value;
                OnPropertyChanged("Row40");
            }
        }

        public int Row41
        {
            get { return boardMatrix[4, 1]; }

             set
            {
                boardMatrix[4, 1] = value;
                OnPropertyChanged("Row41");
            }
        }

        public int Row42
        {
            get { return boardMatrix[4, 2]; }

             set
            {
                boardMatrix[4, 2] = value;
                OnPropertyChanged("Row42");
            }
        }

        public int Row43
        {
            get { return boardMatrix[4, 3]; }

             set
            {
                boardMatrix[4, 3] = value;
                OnPropertyChanged("Row43");
            }
        }

        public int Row44
        {
            get { return boardMatrix[4, 4]; }

             set
            {
                boardMatrix[4, 4] = value;
                OnPropertyChanged("Row44");
            }
        }

        public int Row45
        {
            get { return boardMatrix[4, 5]; }

             set
            {
                boardMatrix[4, 5] = value;
                OnPropertyChanged("Row05");
            }
        }

        public int Row46
        {
            get { return boardMatrix[4, 6]; }

             set
            {
                boardMatrix[4, 6] = value;
                OnPropertyChanged("Row46");
            }
        }

        public int Row47
        {
            get { return boardMatrix[4, 7]; }

             set
            {
                boardMatrix[4, 7] = value;
                OnPropertyChanged("Row47");
            }
        }

        public int Row48
        {
            get { return boardMatrix[4, 8]; }

             set
            {
                boardMatrix[4, 8] = value;
                OnPropertyChanged("Row48");
            }
        }




        #endregion
        #region ROW 6
        public int Row50
        {
            get { return boardMatrix[5, 0]; }

             set
            {
                boardMatrix[5, 0] = value;
                OnPropertyChanged("Row50");
            }
        }

        public int Row51
        {
            get { return boardMatrix[5, 1]; }

             set
            {
                boardMatrix[5, 1] = value;
                OnPropertyChanged("Row51");
            }
        }

        public int Row52
        {
            get { return boardMatrix[5, 2]; }

             set
            {
                boardMatrix[5, 2] = value;
                OnPropertyChanged("Row52");
            }
        }

        public int Row53
        {
            get { return boardMatrix[5, 3]; }

             set
            {
                boardMatrix[5, 3] = value;
                OnPropertyChanged("Row53");
            }
        }

        public int Row54
        {
            get { return boardMatrix[5, 4]; }

             set
            {
                boardMatrix[5, 4] = value;
                OnPropertyChanged("Row54");
            }
        }

        public int Row55
        {
            get { return boardMatrix[5, 5]; }

             set
            {
                boardMatrix[5, 5] = value;
                OnPropertyChanged("Row55");
            }
        }

        public int Row56
        {
            get { return boardMatrix[5, 6]; }

             set
            {
                boardMatrix[5, 6] = value;
                OnPropertyChanged("Row56");
            }
        }

        public int Row57
        {
            get { return boardMatrix[5, 7]; }

             set
            {
                boardMatrix[5, 7] = value;
                OnPropertyChanged("Row57");
            }
        }

        public int Row58
        {
            get { return boardMatrix[5, 8]; }

             set
            {
                boardMatrix[5, 8] = value;
                OnPropertyChanged("Row58");
            }
        }




        #endregion
        #region ROW 7
        public int Row60
        {
            get { return boardMatrix[6, 0]; }

             set
            {
                boardMatrix[6, 0] = value;
                OnPropertyChanged("Row60");
            }
        }

        public int Row61
        {
            get { return boardMatrix[6, 1]; }

             set
            {
                boardMatrix[6, 1] = value;
                OnPropertyChanged("Row61");
            }
        }

        public int Row62
        {
            get { return boardMatrix[6, 2]; }

             set
            {
                boardMatrix[6, 2] = value;
                OnPropertyChanged("Row62");
            }
        }

        public int Row63
        {
            get { return boardMatrix[6, 3]; }

             set
            {
                boardMatrix[6, 3] = value;
                OnPropertyChanged("Row63");
            }
        }

        public int Row64
        {
            get { return boardMatrix[6, 4]; }

             set
            {
                boardMatrix[6, 4] = value;
                OnPropertyChanged("Row64");
            }
        }

        public int Row65
        {
            get { return boardMatrix[6, 5]; }

             set
            {
                boardMatrix[6, 5] = value;
                OnPropertyChanged("Row65");
            }
        }

        public int Row66
        {
            get { return boardMatrix[6, 6]; }

             set
            {
                boardMatrix[6, 6] = value;
                OnPropertyChanged("Row66");
            }
        }

        public int Row67
        {
            get { return boardMatrix[6, 7]; }

             set
            {
                boardMatrix[6, 7] = value;
                OnPropertyChanged("Row67");
            }
        }

        public int Row68
        {
            get { return boardMatrix[6, 8]; }

             set
            {
                boardMatrix[6, 8] = value;
                OnPropertyChanged("Row68");
            }
        }




        #endregion    
        #region ROW 8
        public int Row70
        {
            get { return boardMatrix[7, 0]; }

             set
            {
                boardMatrix[7, 0] = value;
                OnPropertyChanged("Row70");
            }
        }

        public int Row71
        {
            get { return boardMatrix[7, 1]; }

             set
            {
                boardMatrix[7, 1] = value;
                OnPropertyChanged("Row71");
            }
        }

        public int Row72
        {
            get { return boardMatrix[7, 2]; }

             set
            {
                boardMatrix[7, 2] = value;
                OnPropertyChanged("Row72");
            }
        }

        public int Row73
        {
            get { return boardMatrix[7, 3]; }

             set
            {
                boardMatrix[7, 3] = value;
                OnPropertyChanged("Row73");
            }
        }

        public int Row74
        {
            get { return boardMatrix[7, 4]; }

             set
            {
                boardMatrix[7, 4] = value;
                OnPropertyChanged("Row74");
            }
        }

        public int Row75
        {
            get { return boardMatrix[7, 5]; }

             set
            {
                boardMatrix[7, 5] = value;
                OnPropertyChanged("Row75");
            }
        }

        public int Row76
        {
            get { return boardMatrix[7, 6]; }

             set
            {
                boardMatrix[7, 6] = value;
                OnPropertyChanged("Row76");
            }
        }

        public int Row77
        {
            get { return boardMatrix[7, 7]; }

             set
            {
                boardMatrix[7, 7] = value;
                OnPropertyChanged("Row77");
            }
        }

        public int Row78
        {
            get { return boardMatrix[7, 8]; }

             set
            {
                boardMatrix[7, 8] = value;
                OnPropertyChanged("Row78");
            }
        }




        #endregion
        #region ROW 9
        public int Row80
        {
            get { return boardMatrix[8, 0]; }

             set
            {
                boardMatrix[8, 0] = value;
                OnPropertyChanged("Row80");
            }
        }

        public int Row81
        {
            get { return boardMatrix[8, 1]; }

             set
            {
                boardMatrix[8, 1] = value;
                OnPropertyChanged("Row81");
            }
        }

        public int Row82
        {
            get { return boardMatrix[8, 2]; }

             set
            {
                boardMatrix[8, 2] = value;
                OnPropertyChanged("Row82");
            }
        }

        public int Row83
        {
            get { return boardMatrix[8, 3]; }

             set
            {
                boardMatrix[8, 3] = value;
                OnPropertyChanged("Row83");
            }
        }

        public int Row84
        {
            get { return boardMatrix[8, 4]; }

             set
            {
                boardMatrix[8, 4] = value;
                OnPropertyChanged("Row84");
            }
        }

        public int Row85
        {
            get { return boardMatrix[8, 5]; }

             set
            {
                boardMatrix[8, 5] = value;
                OnPropertyChanged("Row85");
            }
        }

        public int Row86
        {
            get { return boardMatrix[8, 6]; }

             set
            {
                boardMatrix[8, 6] = value;
                OnPropertyChanged("Row86");
            }
        }

        public int Row87
        {
            get { return boardMatrix[8, 7]; }

             set
            {
                boardMatrix[8, 7] = value;
                OnPropertyChanged("Row87");
            }
        }

        public int Row88
        {
            get { return boardMatrix[8, 8]; }

             set
            {
                boardMatrix[8, 8] = value;
                OnPropertyChanged("Row88");
            }
        }




        #endregion

        //Hashsets of each row
        private HashSet<int> Row0; private HashSet<int> Row1; private HashSet<int> Row2;
        private HashSet<int> Row3; private HashSet<int> Row4; private HashSet<int> Row5;
        private HashSet<int> Row6; private HashSet<int> Row7; private HashSet<int> Row8;

        //Hashsets of each column
        private HashSet<int> Col0; private HashSet<int> Co11; private HashSet<int> Col2;
        private HashSet<int> Col3; private HashSet<int> Col4; private HashSet<int> Col5;
        private HashSet<int> Col6; private HashSet<int> Col7; private HashSet<int> Col8;


        //Hashset of each 3 by 3 unit
        private HashSet<int> Box0; private HashSet<int> Box1; private HashSet<int> Box2;
        private HashSet<int> Box3; private HashSet<int> Box4; private HashSet<int> Box5;
        private HashSet<int> Box6; private HashSet<int> Box7; private HashSet<int> Box8;


        public QlikSolveVM(int [,] bm)
        {
            possibleValues = new List<int> {1,2,3,4,5,6,7,8,9 };
           
            boardMatrix = bm;
       }


        /// <summary>
        /// Used to validate the sudoku board before passing it to the solve algorithm
        /// </summary>
        /// <param name="board"></param>
        /// <returns>bool</returns>
        //this ensure that the algorith checks each unit of the board only ones
        public bool ValidateWholeBoard(int[,] board)
        {

            #region Row Validation
            Row0 = new HashSet<int>(); Row1 = new HashSet<int>(); Row2 = new HashSet<int>();
            Row3 = new HashSet<int>(); Row4 = new HashSet<int>(); Row5 = new HashSet<int>();
            Row6 = new HashSet<int>(); Row7 = new HashSet<int>(); Row8 = new HashSet<int>();

            for (int i = 0; i < 7; i++)
            {
                switch (i)
                {

                    case 0:
                        for (int j = 0; j < board.GetLength(1); j++)
                        {
                            if (board[i, j] == 0)
                                continue;
                            else if (Row0.Contains(board[i, j]))
                            {
                                return false;
                            }
                            else
                                Row0.Add(board[i, j]);
                        }
                        break;
                    case 1:
                        for (int j = 0; j < board.GetLength(1); j++)
                        {
                            if (board[i, j] == 0)
                                continue;
                            else if (Row1.Contains(board[i, j]))
                            {
                                return false;
                            }
                            else
                                Row1.Add(board[i, j]);
                        }
                        break;
                    case 2:
                        for (int j = 0; j < board.GetLength(1); j++)
                        {
                            if (board[i, j] == 0)
                                continue;
                            else if (Row2.Contains(board[i, j]))
                            {
                                return false;
                            }
                            else
                                Row2.Add(board[i, j]);
                        }
                        break;
                    case 3:
                        for (int j = 0; j < board.GetLength(1); j++)
                        {
                            if (board[i, j] == 0)
                                continue;
                            else if (Row3.Contains(board[i, j]))
                            {
                                return false;
                            }
                            else
                                Row3.Add(board[i, j]);
                        }
                        break;

                    case 4:
                        for (int j = 0; j < board.GetLength(1); j++)
                        {
                            if (board[i, j] == 0)
                                continue;
                            else if (Row4.Contains(board[i, j]))
                            {
                                return false;
                            }
                            else
                                Row4.Add(board[i, j]);
                        }
                        break;


                    case 5:
                        for (int j = 0; j < board.GetLength(1); j++)
                        {
                            if (board[i, j] == 0)
                                continue;
                            else if (Row5.Contains(board[i, j]))
                            {
                                return false;
                            }
                            else
                                Row5.Add(board[i, j]);
                        }
                        break;

                    case 6:
                        for (int j = 0; j < board.GetLength(1); j++)
                        {
                            if (board[i, j] == 0)
                                continue;
                            else if (Row6.Contains(board[i, j]))
                            {
                                return false;
                            }
                            else
                                Row6.Add(board[i, j]);
                        }
                        break;


                    case 7:
                        for (int j = 0; j < board.GetLength(1); j++)
                        {
                            if (board[i, j] == 0)
                                continue;
                            else if (Row7.Contains(board[i, j]))
                            {
                                return false;
                            }
                            else
                                Row7.Add(board[i, j]);
                        }
                        break;

                    case 8:
                        for (int j = 0; j < board.GetLength(1); j++)
                        {
                            if (board[i, j] == 0)
                                continue;
                            else if (Row8.Contains(board[i, j]))
                            {
                                return false;
                            }
                            else
                                Row8.Add(board[i, j]);
                        }
                        break;

                }

            }

            #endregion
            #region Column Validation
            Col0 = new HashSet<int>(); Co11 = new HashSet<int>(); Col2 = new HashSet<int>();
            Col3 = new HashSet<int>(); Col4 = new HashSet<int>(); Col5 = new HashSet<int>();
            Col6 = new HashSet<int>(); Col7 = new HashSet<int>(); Col8 = new HashSet<int>();

            for (int i = 0; i < board.GetLength(0); i++)
            {
                switch (i)
                {

                    case 0:
                        for (int j = 0; j < board.GetLength(1); j++)
                        {
                            if (board[j, i] == 0)
                                continue;
                            else if (Col0.Contains(board[j, i]))
                            {
                                return false;
                            }
                            else
                                Col0.Add(board[j, i]);
                        }
                        break;

                    case 1:
                        for (int j = 0; j < board.GetLength(1); j++)
                        {
                            if (board[j, i] == 0)
                                continue;
                            else if (Co11.Contains(board[j, i]))
                            {
                                return false;
                            }
                            else
                                Co11.Add(board[j, i]);
                        }
                        break;

                    case 2:
                        for (int j = 0; j < board.GetLength(1); j++)
                        {
                            if (board[j, i] == 0)
                                continue;
                            else if (Col2.Contains(board[j, i]))
                            {
                                return false;
                            }
                            else
                                Col2.Add(board[j, i]);
                        }
                        break;

                    case 3:
                        for (int j = 0; j < board.GetLength(1); j++)
                        {
                            if (board[j, i] == 0)
                                continue;
                            else if (Col3.Contains(board[j, i]))
                            {
                                return false;
                            }
                            else
                                Col3.Add(board[j, i]);
                        }
                        break;


                    case 4:
                        for (int j = 0; j < board.GetLength(1); j++)
                        {
                            if (board[j, i] == 0)
                                continue;
                            else if (Col4.Contains(board[j, i]))
                            {
                                return false;
                            }
                            else
                                Col4.Add(board[j, i]);
                        }
                        break;

                    case 5:
                        for (int j = 0; j < board.GetLength(1); j++)
                        {
                            if (board[j, i] == 0)
                                continue;
                            else if (Col5.Contains(board[j, i]))
                            {
                                return false;
                            }
                            else
                                Col5.Add(board[j, i]);
                        }
                        break;

                    case 6:
                        for (int j = 0; j < board.GetLength(1); j++)
                        {
                            if (board[j, i] == 0)
                                continue;
                            else if (Col6.Contains(board[j, i]))
                            {
                                return false;
                            }
                            else
                                Col6.Add(board[j, i]);
                        }
                        break;

                    case 7:
                        for (int j = 0; j < board.GetLength(1); j++)
                        {
                            if (board[j, i] == 0)
                                continue;
                            else if (Col7.Contains(board[j, i]))
                            {
                                return false;
                            }
                            else
                                Col7.Add(board[j, i]);
                        }
                        break;


                    case 8:
                        for (int j = 0; j < board.GetLength(1); j++)
                        {
                            if (board[j, i] == 0)
                                continue;
                            else if (Col8.Contains(board[j, i]))
                            {
                                return false;
                            }
                            else
                                Col8.Add(board[j, i]);
                        }
                        break;

                }

            }
            #endregion
            #region Box Validation
            Box0 = new HashSet<int>(); Box1 = new HashSet<int>(); Box2 = new HashSet<int>();
            Box3 = new HashSet<int>(); Box4 = new HashSet<int>(); Box5 = new HashSet<int>();
            Box6 = new HashSet<int>(); Box7 = new HashSet<int>(); Box8 = new HashSet<int>();
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    if (board[i, j] == 0)
                        continue;
                    else if (Box0.Contains(board[i, j]))
                    {
                        return false;
                    }
                    else
                        Box0.Add(board[i, j]);

                }
            }

            for (int i = 0; i < 3; i++)
            {
                for (int j = 3; j < 6; j++)
                {
                    if (board[i, j] == 0)
                        continue;
                    else if (Box1.Contains(board[i, j]))
                    {
                        return false;
                    }
                    else
                        Box1.Add(board[i, j]);

                }
            }

            for (int i = 0; i < 3; i++)
            {
                for (int j = 6; j < 9; j++)
                {
                    if (board[i, j] == 0)
                        continue;
                    else if (Box2.Contains(board[i, j]))
                    {
                        return false;
                    }
                    else
                        Box2.Add(board[i, j]);

                }
            }

            for (int i = 3; i < 6; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    if (board[i, j] == 0)
                        continue;
                    else if (Box3.Contains(board[i, j]))
                    {
                        return false;
                    }
                    else
                        Box3.Add(board[i, j]);

                }
            }

            for (int i = 3; i < 6; i++)
            {
                for (int j = 3; j < 6; j++)
                {
                    if (board[i, j] == 0)
                        continue;
                    else if (Box4.Contains(board[i, j]))
                    {
                        return false;
                    }
                    else
                        Box4.Add(board[i, j]);

                }
            }

            for (int i = 3; i < 6; i++)
            {
                for (int j = 6; j < 9; j++)
                {
                    if (board[i, j] == 0)
                        continue;
                    else if (Box5.Contains(board[i, j]))
                    {
                        return false;
                    }
                    else
                        Box5.Add(board[i, j]);

                }
            }

            for (int i = 6; i < 9; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    if (board[i, j] == 0)
                        continue;
                    else if (Box6.Contains(board[i, j]))
                    {
                        return false;
                    }
                    else
                        Box6.Add(board[i, j]);

                }
            }

            for (int i = 6; i < 9; i++)
            {
                for (int j = 3; j < 6; j++)
                {
                    if (board[i, j] == 0)
                        continue;
                    else if (Box7.Contains(board[i, j]))
                    {
                        return false;
                    }
                    else
                        Box7.Add(board[i, j]);

                }
            }

            for (int i = 6; i < 9; i++)
            {
                for (int j = 6; j < 9; j++)
                {
                    if (board[i, j] == 0)
                        continue;
                    else if (Box8.Contains(board[i, j]))
                    {
                        return false;
                    }
                    else
                        Box8.Add(board[i, j]);

                }
            }
            #endregion


            return true;
        }

        /// <summary>
        /// Depth First Search Algorith .EMploys the use of backtracking through recursion
        /// </summary>
        /// <param name="board"></param>
        /// <param name="index"></param>
        /// <returns></returns>

        private  void SolveBoard( int[,] board, int index)
        {
            int totalSize = board.Length;
            int S = board.GetLength(0);
            if (index == totalSize)
            {
                
              
                MessagingCenter.Send<QlikSolveVM, int[,]>(this, "PushPage", board);
          

                IsLoading = false;
              

            }
            else
            {
                int row = index / S;
                int col = index % S;

                if (board[row, col] != 0)
                    SolveBoard(board, index + 1);
                else
                {
                    for (int i = 1; i <= 9; i++)
                    {
                        if (ValidateForIndexval(board, row, col, i))
                        {
                            board[row, col] = i;
                            SolveBoard(board, index + 1);
                            board[row, col] = 0;

                        }

                    }
                }
            }

        }
/*
        public static async Task<DateTime> SolveBoardAsync(int[,] board, int index)
        {

           // await Task.Run(() => SolveBoard(board, index));
            return DateTime.Now;

        }
*/

        //validation for each specific index (did not use this to validate the board in order to avoid cheking the same unit more than ones
        private bool ValidateForIndexval(int[,] board, int row, int col, int indexVal)
        {
            int S = board.GetLength(0);

            ///check row and column units to see if the index value is reapeated
            for (int i = 0; i < S; i++)
            {
                if (board[row, i] == indexVal)
                    return false;

                if (board[i, col] == indexVal)
                    return false;
            }

            int rowStart = row - row % 3;
            int colStart = col - col % 3;

            for (int j = 0; j < 3; j++)
            {
                for (int k = 0; k < 3; k++)
                {
                    if (board[rowStart + k, colStart + j] == indexVal)
                        return false;
                }
            }
            return true;
            ///check box to see if the index value is reapeated

        }

        //This is the function that the application will call to solve the sudoku board
        public void RunSolveSudoku()
        {
        
            solvedBoard = (int[,])boardMatrix.Clone();
            SolveBoard(solvedBoard, 0);

       }

        public bool HasAllZeros(int [,] board)
        {

            for (int i=0;i<board.GetLength(0);i++) {
             for (int j=0; j < board.GetLength(0); j++)
                {
                    if (board[i, j] != 0)
                        return false;
                }

            }
            return true;
        }


        public bool HasNoZeros(int[,] board)
        {

            for (int i = 0; i < board.GetLength(0); i++)
            {
                for (int j = 0; j < board.GetLength(0); j++)
                {
                    if (board[i, j] == 0)
                        return false;
                }

            }
            return true;
        }
        // Tell the view to update its values
        void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this,
                    new PropertyChangedEventArgs(propertyName));
        }
    }
}
