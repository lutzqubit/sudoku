﻿// ***********************************************************************
// Assembly         : QlikNSolveSudoku
// Author           : Lutse Imobekhai
// Created          : 10-25-2016
// 
// Last Modified By : Lutse Imobekhai
// Last Modified On : 10-30-2016
// ***********************************************************************
//
// <summary>
//       This project is located at
//      https://bitbucket.org/lutzqubit/sudoku/src
//     Same sudoku engine in the test project
// </summary>
// ***********************************************************************
// 



using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace QlikNSolveSudoku.ViewModel
{
   public class SudokuEngine
    {
        //Hashsets of each row
        private  HashSet<int> Row0; private HashSet<int> Row1; private HashSet<int> Row2;
        private HashSet<int> Row3; private HashSet<int> Row4; private HashSet<int> Row5;
        private HashSet<int> Row6; private HashSet<int> Row7; private HashSet<int> Row8;

        //Hashsets of each column
        private HashSet<int> Col0; private HashSet<int> Co11; private HashSet<int> Col2;
        private HashSet<int> Col3; private HashSet<int> Col4; private HashSet<int> Col5;
        private HashSet<int> Col6; private HashSet<int> Col7; private HashSet<int> Col8;


        //Hashset of each 3 by 3 unit
        private HashSet<int> Box0; private HashSet<int> Box1; private HashSet<int> Box2;
        private HashSet<int> Box3; private HashSet<int> Box4; private HashSet<int> Box5;
        private HashSet<int> Box6; private HashSet<int> Box7; private HashSet<int> Box8;



        public SudokuEngine()
        {
         


        }

        /// <summary>
        /// Used to validate the sudoku board before passing it to the solve algorithm
        /// </summary>
        /// <param name="board"></param>
        /// <returns>bool</returns>
        //this ensure that the algorith checks each unit of the board only ones
        public bool ValidateForConsistency(int [,] board)
        {
            
            #region Row Validation
            Row0 = new HashSet<int>(); Row1 = new HashSet<int>(); Row2 = new HashSet<int>();
            Row3 = new HashSet<int>(); Row4 = new HashSet<int>(); Row5 = new HashSet<int>();
            Row6 = new HashSet<int>(); Row7 = new HashSet<int>(); Row8 = new HashSet<int>();

            for (int i = 0; i <7; i++)
            {
                switch (i)
                {
                    
                    case 0:
                        for (int j = 0; j < board.GetLength(1); j++)
                        {
                            if (board[i, j] == 0)
                                continue;
                            else if (Row0.Contains(board[i, j]))
                            {
                                return false;
                            }
                            else
                                Row0.Add(board[i, j]);
                        }
                        break;
                                 case 1:
                                      for (int j = 0; j < board.GetLength(1); j++)
                                      {
                                          if (board[i, j] == 0)
                                              continue;
                                          else if (Row1.Contains(board[i, j]))
                                          {
                                              return false;
                                          }
                                          else
                                              Row1.Add(board[i, j]);
                                      }
                                      break; 
                                   case 2:
                                       for (int j = 0; j < board.GetLength(1); j++)
                                       {
                                           if (board[i, j] == 0)
                                               continue;
                                           else if (Row2.Contains(board[i, j]))
                                           {
                                               return false;
                                           }
                                           else
                                               Row2.Add(board[i, j]);
                                       }
                                       break; 
                                 case 3:
                                   for (int j = 0; j < board.GetLength(1); j++)
                                   {
                                       if (board[i, j] == 0)
                                           continue;
                                       else if (Row3.Contains(board[i, j]))
                                       {
                                           return false;
                                       }
                                       else
                                           Row3.Add(board[i, j]);
                                   }
                                   break;
                                   
                                      case 4:
                                         for (int j = 0; j < board.GetLength(1); j++)
                                         {
                                             if (board[i, j] == 0)
                                                 continue;
                                             else if (Row4.Contains(board[i, j]))
                                             {
                                                 return false;
                                             }
                                             else
                                                 Row4.Add(board[i, j]);
                                         }
                                         break;
                 
                        
                                            case 5:
                                                for (int j = 0; j < board.GetLength(1); j++)
                                                {
                                                    if (board[i, j] == 0)
                                                        continue;
                                                    else if (Row5.Contains(board[i, j]))
                                                    {
                                                        return false;
                                                    }
                                                    else
                                                        Row5.Add(board[i, j]);
                                                }
                                                break;
                        
                                       case 6:
                                           for (int j = 0; j < board.GetLength(1); j++)
                                           {
                                               if (board[i, j] == 0)
                                                   continue;
                                               else if (Row6.Contains(board[i, j]))
                                               {
                                                   return false;
                                               }
                                               else
                                                   Row6.Add(board[i, j]);
                                           }
                                           break;
                   
                        
                                            case 7:
                                                for (int j = 0; j < board.GetLength(1); j++)
                                                {
                                                    if (board[i, j] == 0)
                                                        continue;
                                                    else if (Row7.Contains(board[i, j]))
                                                    {
                                                        return false;
                                                    }
                                                    else
                                                        Row7.Add(board[i, j]);
                                                }
                                                break;
                        
                                       case 8 :
                                          for (int j = 0; j < board.GetLength(1); j++)
                                          {
                                              if (board[i, j] == 0)
                                                  continue;
                                              else if (Row8.Contains(board[i, j]))
                                              {
                                                  return false;
                                              }
                                              else
                                                  Row8.Add(board[i, j]);
                                          }
                                          break;
                  
                }

            }

            #endregion
            #region Column Validation
            Col0 = new HashSet<int>(); Co11 = new HashSet<int>(); Col2 = new HashSet<int>();
            Col3 = new HashSet<int>(); Col4 = new HashSet<int>(); Col5 = new HashSet<int>();
            Col6 = new HashSet<int>(); Col7 = new HashSet<int>(); Col8 = new HashSet<int>();

            for (int i = 0; i < board.GetLength(0); i++)
            {
                switch (i)
                {

                    case 0:
                        for (int j = 0; j < board.GetLength(1); j++)
                        {
                            if (board[j, i] == 0)
                                continue;
                            else if (Col0.Contains(board[j, i]))
                            {
                                return false;
                            }
                            else
                                Col0.Add(board[j, i]);
                        }
                        break;

                    case 1:
                        for (int j = 0; j < board.GetLength(1); j++)
                        {
                            if (board[j, i] == 0)
                                continue;
                            else if (Co11.Contains(board[j, i]))
                            {
                                return false;
                            }
                            else
                                Co11.Add(board[j, i]);
                        }
                        break;

                    case 2:
                        for (int j = 0; j < board.GetLength(1); j++)
                        {
                            if (board[j, i] == 0)
                                continue;
                            else if (Col2.Contains(board[j, i]))
                            {
                                return false;
                            }
                            else
                                Col2.Add(board[j, i]);
                        }
                        break;

                    case 3:
                        for (int j = 0; j < board.GetLength(1); j++)
                        {
                            if (board[j, i] == 0)
                                continue;
                            else if (Col3.Contains(board[j, i]))
                            {
                                return false;
                            }
                            else
                                Col3.Add(board[j, i]);
                        }
                        break;


                    case 4:
                        for (int j = 0; j < board.GetLength(1); j++)
                        {
                            if (board[j, i] == 0)
                                continue;
                            else if (Col4.Contains(board[j, i]))
                            {
                                return false;
                            }
                            else
                                Col4.Add(board[j, i]);
                        }
                        break;

                    case 5:
                        for (int j = 0; j < board.GetLength(1); j++)
                        {
                            if (board[j, i] == 0)
                                continue;
                            else if (Col5.Contains(board[j, i]))
                            {
                                return false;
                            }
                            else
                                Col5.Add(board[j, i]);
                        }
                        break;

                    case 6:
                        for (int j = 0; j < board.GetLength(1); j++)
                        {
                            if (board[j, i] == 0)
                                continue;
                            else if (Col6.Contains(board[j, i]))
                            {
                                return false;
                            }
                            else
                                Col6.Add(board[j, i]);
                        }
                        break;

                    case 7:
                        for (int j = 0; j < board.GetLength(1); j++)
                        {
                            if (board[j, i] == 0)
                                continue;
                            else if (Col7.Contains(board[j, i]))
                            {
                                return false;
                            }
                            else
                                Col7.Add(board[j, i]);
                        }
                        break;


                     case 8:
                        for (int j = 0; j < board.GetLength(1); j++)
                        {
                            if (board[j, i] == 0)
                                continue;
                            else if (Col8.Contains(board[j, i]))
                            {
                                return false;
                            }
                            else
                                Col8.Add(board[j, i]);
                        }
                        break;
                       
                }

            }
            #endregion
            #region Box Validation
            Box0 = new HashSet<int>(); Box1 = new HashSet<int>(); Box2 = new HashSet<int>();
            Box3 = new HashSet<int>(); Box4 = new HashSet<int>(); Box5 = new HashSet<int>();
            Box6 = new HashSet<int>(); Box7 = new HashSet<int>(); Box8 = new HashSet<int>();
            for (int i=0; i < 3; i++)
            {
              for (int j = 0; j < 3; j++)
                {
                    if (board[i, j] == 0)
                        continue;
                    else if (Box0.Contains(board[i, j]))
                    {
                        return false;
                    }
                    else
                        Box0.Add(board[i, j]);

                }
            }

            for (int i = 0; i < 3; i++)
            {
                for (int j = 3; j < 6; j++)
                {
                    if (board[i, j] == 0)
                        continue;
                    else if (Box1.Contains(board[i, j]))
                    {
                        return false;
                    }
                    else
                        Box1.Add(board[i, j]);

                }
            }

            for (int i = 0; i < 3; i++)
            {
                for (int j = 6; j < 9; j++)
                {
                    if (board[i, j] == 0)
                        continue;
                    else if (Box2.Contains(board[i, j]))
                    {
                        return false;
                    }
                    else
                        Box2.Add(board[i, j]);

                }
            }

            for (int i = 3; i < 6; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    if (board[i, j] == 0)
                        continue;
                    else if (Box3.Contains(board[i, j]))
                    {
                        return false;
                    }
                    else
                        Box3.Add(board[i, j]);

                }
            }

            for (int i = 3; i < 6; i++)
            {
                for (int j = 3; j < 6; j++)
                {
                    if (board[i, j] == 0)
                        continue;
                    else if (Box4.Contains(board[i, j]))
                    {
                        return false;
                    }
                    else
                        Box4.Add(board[i, j]);

                }
            }

            for (int i = 3; i < 6; i++)
            {
                for (int j = 6; j < 9; j++)
                {
                    if (board[i, j] == 0)
                        continue;
                    else if (Box5.Contains(board[i, j]))
                    {
                        return false;
                    }
                    else
                        Box5.Add(board[i, j]);

                }
            }

            for (int i = 6; i < 9; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    if (board[i, j] == 0)
                        continue;
                    else if (Box6.Contains(board[i, j]))
                    {
                        return false;
                    }
                    else
                        Box6.Add(board[i, j]);

                }
            }

            for (int i = 6; i < 9; i++)
            {
                for (int j = 3; j < 6; j++)
                {
                    if (board[i, j] == 0)
                        continue;
                    else if (Box7.Contains(board[i, j]))
                    {
                        return false;
                    }
                    else
                        Box7.Add(board[i, j]);

                }
            }

            for (int i = 6; i < 9; i++)
            {
                for (int j = 6; j < 9; j++)
                {
                    if (board[i, j] == 0)
                        continue;
                    else if (Box8.Contains(board[i, j]))
                    {
                        return false;
                    }
                    else
                        Box8.Add(board[i, j]);

                }
            }
            #endregion


            return true;
        }

        /// <summary>
        /// Depth First Search Algorith .EMploys the use of backtracking through recursion
        /// </summary>
        /// <param name="board"></param>
        /// <param name="index"></param>
        /// <returns></returns>

        public static void SolveBoard(  int[,] board , int index)
        {
              int totalSize = board.Length;
            int S = board.GetLength(0);
            if (index == totalSize)
            {
                Debug.WriteLine("DOne Done Done");

            } 
            else
            {
                int row = index / S;
                int col = index % S;

                if (board[row, col] != 0)
                    SolveBoard(board, index + 1);
                else
                {
                    for (int i = 1; i <= 9; i++)
                    {
                        if (validateForIndexval(board, row, col, i))
                        {
                            board[row, col] = i;
                            SolveBoard(board, index + 1);
                            board[row, col] = 0;

                        }

                    }
                }
            }

        }
/*
        public static async Task <DateTime> SolveBoardAsync(int[,] board, int index)
        {
            
            await Task.Run(() => SolveBoard(  board, index));
            return DateTime.Now;

        }

    */
        //validation for each specific index (did not use this to validate the board in order to avoid cheking the same unit more than ones
        private static bool validateForIndexval(int[,] board, int row, int col, int indexVal)
        {
            int S = board.GetLength(0);
           
            ///check row and column units to see if the index value is reapeated
            for(int i = 0; i < S; i++)
            {
                if (board[row, i] == indexVal)
                    return false;

                if (board[i, col] == indexVal)
                    return false;
            }

            int rowStart = row - row % 3;
            int colStart = col - col % 3;

            for (int j = 0; j < 3; j++)
            {
                for (int k = 0; k < 3; k++)
                {
                    if (board[rowStart + k, colStart + j] == indexVal)
                        return false;
                }
            }
            return true;
            ///check box to see if the index value is reapeated

        }




    }
}
