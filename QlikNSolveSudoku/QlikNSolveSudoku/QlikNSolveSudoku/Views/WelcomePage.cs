﻿// ***********************************************************************
// Assembly         : QlikNSolveSudoku
// Author           : Lutse Imobekhai
// Created          : 10-25-2016
// 
// Last Modified By : Lutse Imobekhai
// Last Modified On : 10-30-2016
// ***********************************************************************
//
// <summary>
//       This project is located at
//      https://bitbucket.org/lutzqubit/sudoku/src
//      The welcome page view
// </summary>
// ***********************************************************************
// 


using System;

using Xamarin.Forms;
using System.Threading.Tasks;

namespace QlikNSolveSudoku
{
	public class WelcomePage : ContentPage
	{   Task task3;
		Image background;
		RelativeLayout layout;
		Image terminalEmuText;
        public ActivityIndicator ActivityIndicator { get { return activityIndicator; } set { activityIndicator = value; } }

        public ActivityIndicator activityIndicator;
        public WelcomePage ()
		{

            activityIndicator = new ActivityIndicator
            {
                Color = Color.Green,
                Scale = 4,
                IsEnabled = true,
                IsRunning = true,
                IsVisible = false,
                BindingContext = this,
            };

            BackgroundColor = Color.Transparent;
			 background = new Image {
				Source = "DefaultPortrait.png",
				Aspect = Aspect.AspectFill,
			};

			terminalEmuText = new Image {
				Source = "SudokuText.png",
				Aspect = Aspect.AspectFit,
				WidthRequest=250,
				HeightRequest=86
			};

			layout = new RelativeLayout ();
			layout.Children.Add (background, 
				xConstraint: Constraint.Constant(0), 
				yConstraint: Constraint.Constant(0), 
				widthConstraint: Constraint.RelativeToParent ((parent) => {return parent.Width;}),
				heightConstraint: Constraint.RelativeToParent ((parent) => {return parent.Height;}));
			layout.Children.Add (terminalEmuText, 
				xConstraint: Constraint.RelativeToParent ((parent) => {
					return (parent.Width / 2)-176; // center of image (which is 40 wide)
				}),

				yConstraint: Constraint.RelativeToParent ((parent) => {
					return (parent.Height/2)+20;
				}));

            layout.Children.Add(activityIndicator,
              xConstraint: Constraint.RelativeToParent((parent) => {
                  return (parent.Width / 2); // center of image (which is 40 wide)
                }),
              yConstraint: Constraint.RelativeToParent((parent) => {
                  return (parent.Height / 2);
              }));
            Content = layout;

		}

		protected override async void OnAppearing()  
		{  
			base.OnAppearing();  

			await background.ScaleTo (2, 1500, Easing.SinInOut);
		  
			await background.ScaleTo (1, 1500, Easing.SinInOut);
			  
		}  




	}
}


