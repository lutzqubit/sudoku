﻿// ***********************************************************************
// Assembly         : QlikNSolveSudoku
// Author           : Lutse Imobekhai
// Created          : 10-25-2016
// 
// Last Modified By : Lutse Imobekhai
// Last Modified On : 10-30-2016
// ***********************************************************************
//
// <summary>
//       This project is located at
//      https://bitbucket.org/lutzqubit/sudoku/src
//      The sudoku grid control that scales to fit screen
// </summary>
// ***********************************************************************
// 


using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;

using Xamarin.Forms;

namespace QlikNSolveSudoku.Views
{
    public class SudokuGrid : Grid
    {
        public SudokuGrid()
        {

            double cellSize=(App.ScreenSize.Width<App.ScreenSize.Height) ? App.ScreenSize.Width / 12 : App.ScreenSize.Height / 20;

            RowDefinition RowHeight= new RowDefinition { Height = new GridLength(cellSize, GridUnitType.Absolute) };
            ColumnDefinition ColHeight = new ColumnDefinition { Width = new GridLength(cellSize, GridUnitType.Absolute) };

            for (int i = 0; i < 10; i++)
            {
                RowDefinitions.Add(RowHeight);
            }

            for (int i = 0; i < 9; i++)
            {
                ColumnDefinitions.Add(ColHeight);
            }
        }
    }
}
