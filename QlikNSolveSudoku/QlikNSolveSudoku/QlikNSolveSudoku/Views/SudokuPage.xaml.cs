﻿// ***********************************************************************
// Assembly         : QlikNSolveSudoku
// Author           : Lutse Imobekhai
// Created          : 10-25-2016
// 
// Last Modified By : Lutse Imobekhai
// Last Modified On : 10-30-2016
// ***********************************************************************
//
// <summary>
//       This project is located at
//      https://bitbucket.org/lutzqubit/sudoku/src
//      The sudoku page view with scaled grid
// </summary>
// ***********************************************************************
// 

using QlikNSolveSudoku.ViewModel;
using QlikNSolveSudoku.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace QlikNSolveSudoku.Views
{
    public partial class SudokuPage : ContentPage 
    {
        QlikSolveVM viewModel;
        QlikSolveVM solutionViewModel;
        SudokuEngine sudokuEngine;
        int[,] sudokuBoard;
        public SudokuPage(int[,] boardMatrix)
        {
           
            viewModel = new QlikSolveVM(boardMatrix);



            InitializeComponent();
            this.BindingContext = viewModel;
            NavigationPage.SetHasBackButton(this, false);
            MessagingCenter.Subscribe<QlikSolveVM, int[,]>(this, "PushPage", (vm, board) => { this.Content = new SolutionPage(board).Content;


                                                                                                     });
          

        }


     async void OnButtonClicked(object sender, EventArgs args)
        {
           

            if (viewModel.ValidateWholeBoard(viewModel.BoardMatrix) && !viewModel.HasAllZeros(viewModel.BoardMatrix))
            {
               
              viewModel.RunSolveSudoku();
              
            }
            else
              await    DisplayAlert("Alert", "Current board is invalid", "OK");
        }



        async void OnBackButtonClicked(object sender, EventArgs args)
        {
            await Navigation.PopToRootAsync();
        }


    }
}
