﻿// ***********************************************************************
// Assembly         : QlikNSolveSudoku
// Author           : Lutse Imobekhai
// Created          : 10-25-2016
// 
// Last Modified By : Lutse Imobekhai
// Last Modified On : 10-30-2016
// ***********************************************************************
//
// <summary>
//       This project is located at
//      https://bitbucket.org/lutzqubit/sudoku/src
//      The loading screen
// </summary>
// ***********************************************************************
// 



using System;

using Xamarin.Forms;
using System.Threading.Tasks;

namespace QlikNSolveSudoku.Views
{
	public class LoadingScreen : ContentPage
	{   Task task3;
		Image background;
		RelativeLayout layout;
		Button button;
		public LoadingScreen ()
		{

            var activityIndicator = new ActivityIndicator
            {
                Color = Color.Green,
                Scale = 4,
                IsEnabled = true,
                IsRunning = true,
                IsVisible = true,
                BindingContext = this,
            };

            background = new Image {
				Source = ImageSource.FromFile("TerminalAnim.png"),
				Aspect = Aspect.Fill,
			};

			button = new Button
            {
                Text = "Loading ",
                HorizontalOptions = LayoutOptions.CenterAndExpand,
                VerticalOptions = LayoutOptions.CenterAndExpand,
            };

            layout = new RelativeLayout ();
			layout.Children.Add (background, 
				xConstraint: Constraint.Constant(0), 
				yConstraint: Constraint.Constant(0), 
				widthConstraint: Constraint.RelativeToParent ((parent) => {return parent.Width;}),
				heightConstraint: Constraint.RelativeToParent ((parent) => {return parent.Height;}));
			layout.Children.Add (button, 
				xConstraint: Constraint.RelativeToParent ((parent) => {
					return (parent.Width/2)+50; // center of image (which is 40 wide)
				}),
				yConstraint: Constraint.RelativeToParent ((parent) => {
					return (parent.Height/2)-73;
				}));
            layout.Children.Add(activityIndicator,
                xConstraint: Constraint.RelativeToParent((parent) => {
                    return (parent.Width / 2); // center of image (which is 40 wide)
                }),
                yConstraint: Constraint.RelativeToParent((parent) => {
                    return (parent.Height / 2);
                }));

            Content = layout;

		}

		protected override async void OnAppearing()  
		{  
			base.OnAppearing();
            await  button.ScaleTo(3, 1000, Easing.CubicInOut);
            await button.RotateTo(15, 1000, Easing.CubicInOut);
            await button.ScaleTo(1, 1000, Easing.CubicInOut);
            await button.RotateTo(0, 1000, Easing.CubicInOut);
            await background.ScaleTo (2, 1500, Easing.SinInOut);
		  
			while (true) {
				
				await background.TranslateTo (0, 20, 5, null);

				await background.TranslateTo (0, -20, 5, null);
			}
			  
		}  




	}
}


