﻿// ***********************************************************************
// Assembly         : QlikNSolveSudoku
// Author           : Lutse Imobekhai
// Created          : 10-25-2016
// 
// Last Modified By : Lutse Imobekhai
// Last Modified On : 10-30-2016
// ***********************************************************************
//
// <summary>
//       This project is located at
//      https://bitbucket.org/lutzqubit/sudoku/src
//      The menu page view
// </summary>
// ***********************************************************************
// 


using QlikNSolveSudoku.Model;
using QlikNSolveSudoku.Views;
using System;
using System.Collections.Generic;
using Xamarin.Forms;


namespace QlikNSolveSudoku
{
	public class MenuPage : ContentPage
	{
        public ListView ListView { get { return listView; } }
        public ActivityIndicator ActivityIndicator { get { return activityIndicator; } set { activityIndicator = value; } }

        public ActivityIndicator activityIndicator;
        ListView listView;


        public MenuPage ()
		{
          
            BoardModel bm = new BoardModel();
            activityIndicator = new ActivityIndicator
            {
                Color = Color.Green,
                IsVisible=false,
                IsRunning=true,
                
                 
            };
            
         
            var top = new Image {
				Source="top.png",
				Aspect= Aspect.AspectFill,
				Opacity=0.9
			};
            

            var masterPageItems = new List<MasterPageItem>();
         
            masterPageItems.Add(new MasterPageItem
            {
                Title = "New Board",
                TargetType = typeof(SudokuPage),
                Difficulty=bm.SudokuBoard
            });
            masterPageItems.Add(new MasterPageItem
            {
                Title = "Easy Board",
                TargetType = typeof(SudokuPage),
                Difficulty= bm.EasySudokuBoard
            });
            masterPageItems.Add(new MasterPageItem
            {
                Title = "Medium Board",
                TargetType = typeof(SudokuPage),
                Difficulty= bm.MediumSudokuBoard
            });
            masterPageItems.Add(new MasterPageItem
            {
                Title = "Hard Board",
                TargetType = typeof(SudokuPage),
                Difficulty = bm.HardSudokuBoard
            });

            listView = new ListView
            {
                HorizontalOptions=LayoutOptions.Center,
                ItemsSource = masterPageItems,
                ItemTemplate = new DataTemplate(() => {
                    var textCell = new TextCell();
                    textCell.SetBinding(TextCell.TextProperty, "Title");
                    textCell.TextColor = Color.Black;
                    return textCell;
                }),
                VerticalOptions = LayoutOptions.FillAndExpand,
                SeparatorVisibility = SeparatorVisibility.Default,
               
            };

            if (Device.OS == TargetPlatform.iOS)
                Padding = new Thickness(20, 40, 0, 0);
          

            Icon = "menu.png";
            Title = "Personal Organiser";
            var semiTransparentColor = new Color(91, 194, 54, 0.5);
            BackgroundColor = semiTransparentColor;
           
            Content = new StackLayout
            {
                VerticalOptions = LayoutOptions.FillAndExpand,
                Children = {
                    top,
                    activityIndicator,
                listView,
               
            }
            };

        }
	}
}


