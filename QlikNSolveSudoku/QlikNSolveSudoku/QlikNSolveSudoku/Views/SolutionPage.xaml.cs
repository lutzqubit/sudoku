﻿using QlikNSolveSudoku.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace QlikNSolveSudoku.Views
{
    public partial class SolutionPage : ContentPage
    {
    
        
        public SolutionPage(int[,] resultMatrix)
        {
            //VM =viewModel;
         
            InitializeComponent();
            Padding = new Thickness(0, Device.OnPlatform(60, 40, 40), 0, 0);

            for (int i = 0; i < resultMatrix.GetLength(0); i++)
            {
                for (int j = 0; j < resultMatrix.GetLength(1); j++)
                {
                    Label gridLabel = new Label
                    {
                        FontSize = Device.GetNamedSize(NamedSize.Large, typeof(Label)),
                        HorizontalTextAlignment = TextAlignment.Center,
                        VerticalTextAlignment = TextAlignment.Center,

                    };
                    gridLabel.Text = resultMatrix[i, j].ToString();
                    if (j > 2 && j < 6 && (i < 3 || i > 5))
                        gridLabel.BackgroundColor = Color.FromHex("#ffa500");
                    else if (i > 2 && i < 6 && (j < 3 || j > 5))
                        gridLabel.BackgroundColor = Color.FromHex("#ffa500");
                    else
                       gridLabel.BackgroundColor = Color.FromHex("#05630c");

                    controlGrid2.Children.Add(gridLabel,j,i);
                }
            }
          
        }
    }
}
