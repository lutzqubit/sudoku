﻿using QlikNSolveSudoku.Model;
using QlikNSolveSudoku.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace QlikNSolveSudoku
{
    public class MyNavigationPage : NavigationPage
    {

        public MyNavigationPage(Page page) : base(page)
        {
            BarBackgroundColor = Color.FromHex("4c86fb");
            Opacity = 0.8;
            Icon = "menu";
        }
    }

    public class App : Application
        {
        public static Size ScreenSize;
           public static MasterDetailPage masterDetailPage;
           private MenuPage masterPage;
          private WelcomePage welcomePage;

        public App()
            {
            BoardModel bm = new BoardModel();
            masterPage = new MenuPage();
            welcomePage = new WelcomePage();
                masterDetailPage = new MasterDetailPage
                {
                    Title = "",
                  //  Icon = "",
                    Master = masterPage,
                    Detail = new NavigationPage(welcomePage)
                };
                MainPage = masterDetailPage;
            masterPage.ListView.ItemSelected += OnItemSelected;
            }

        private async void OnItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
           

             var item = e.SelectedItem as MasterPageItem;
            if (item != null)
            {
                await welcomePage.Navigation.PushAsync(new LoadingScreen());

                await   welcomePage.Navigation.PushAsync((Page)Activator.CreateInstance(item.TargetType,new Object[] { item.Difficulty}));
                masterPage.ListView.SelectedItem = null;
                masterDetailPage.IsPresented = false;
              
            }
        }

        protected override void OnStart()
            {
                // Handle when your app starts
            }

            protected override void OnSleep()
            {
                // Handle when your app sleeps
            }

            protected override void OnResume()
            {
                // Handle when your app resumes
            }
        }
 
}
