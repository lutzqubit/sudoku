﻿// ***********************************************************************
// Assembly         : QlikNSolveSudoku
// Author           : Lutse Imobekhai
// Created          : 10-25-2016
// 
// Last Modified By : Lutse Imobekhai
// Last Modified On : 10-30-2016
// ***********************************************************************
//
// <summary>
//       This project is located at
//      https://bitbucket.org/lutzqubit/sudoku/src
//      It contains all Empty, Easy, Medium and Hard Board model formats
// </summary>
// ***********************************************************************
// 




using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QlikNSolveSudoku.Model
{
   

    class BoardModel
    {

        //Gets and sets the current instance of the Sudoku Array
        private int[,] sudokuBoard;     
        public int[,] SudokuBoard
        {
            get { return sudokuBoard; }
            set { sudokuBoard = value; }
        }

        //Gets the easy sudoku board (sample generated from www.sudoku-solutions.com)
        private int[,] easySudokuBoard;
        public int[,] EasySudokuBoard
        {
            get { return easySudokuBoard; }
          
        }

        //Gets the medium sudoku board (sample generated from www.sudoku-solutions.com)
        private int[,] mediumSudokuBoard;
        public int[,] MediumSudokuBoard
        {
            get { return mediumSudokuBoard; }

        }

        //Gets the hard sudoku board (sample generated from www.sudoku-solutions.com)
        private int[,] hardSudokuBoard;
        public int[,] HardSudokuBoard
        {
            get { return hardSudokuBoard; }

        }



        public BoardModel()
        {
          

                sudokuBoard = new int[9, 9] { { 0,0,0,0,0,0,0,0,0 },
                                              { 0,0,0,0,0,0,0,0,0 },
                                              { 0,0,0,0,0,0,0,0,0 },
                                              { 0,0,0,0,0,0,0,0,0 },
                                              { 0,0,0,0,0,0,0,0,0 },
                                              { 0,0,0,0,0,0,0,0,0 },
                                              { 0,0,0,0,0,0,0,0,0 },
                                              { 0,0,0,0,0,0,0,0,0 },
                                              { 0,0,0,0,0,0,0,0,0 },};


            easySudokuBoard = new int[9, 9] { { 0,0,0,0,0,9,7,6,0 },
                                              { 0,7,0,4,0,0,1,2,0 },
                                              { 0,0,4,0,0,0,0,0,0 },
                                              { 0,2,7,3,6,0,8,9,0 },
                                              { 0,0,0,2,0,7,0,0,0 },
                                              { 0,4,3,0,9,5,2,1,0 },
                                              { 0,0,0,0,0,0,5,0,0 },
                                              { 0,6,5,0,0,3,0,4,0 },
                                              { 0,3,9,7,0,0,0,0,0 },};

          hardSudokuBoard = new int[9, 9] { { 7,0,1,0,2,5,0,4,0 },
                                              { 0,0,5,6,1,0,2,0,0 },
                                              { 6,0,0,0,9,0,0,0,0 },
                                              { 0,7,0,0,0,0,0,0,0 },
                                              { 0,0,2,0,0,0,0,3,0 },
                                              { 0,0,0,0,0,8,0,0,0 },
                                              { 0,0,7,0,0,0,3,9,0 },
                                              { 1,0,0,9,5,0,0,0,4 },
                                              { 0,0,9,0,3,2,0,1,7 },};

            mediumSudokuBoard = new int[9, 9] { { 4,0,0,9,0,5,0,1,0 },
                                              { 5,9,0,3,0,0,4,0,0 },
                                              { 0,0,1,0,0,0,0,0,0 },
                                              { 0,2,0,7,0,0,0,0,1 },
                                              { 0,1,4,6,0,2,5,8,0 },
                                              { 3,0,0,0,0,1,0,4,0 },
                                              { 0,0,0,0,0,0,7,0,0 },
                                              { 0,0,7,0,0,9,0,5,6 },
                                              { 0,5,0,4,0,7,0,0,9 },};


        }
    }
}
