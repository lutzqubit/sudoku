﻿// ***********************************************************************
// Assembly         : QlikNSolveSudoku
// Author           : Lutse Imobekhai
// Created          : 10-25-2016
// 
// Last Modified By : Lutse Imobekhai
// Last Modified On : 10-30-2016
// ***********************************************************************
//
// <summary>
//       This project is located at
//      https://bitbucket.org/lutzqubit/sudoku/src
//      Options  for the Master page menu
// </summary>
// ***********************************************************************
// 



using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;


/// <summary>
/// This namespace holds all the models that would be used as static user interface content
/// </summary>
namespace QlikNSolveSudoku.Model
{
    // Holds all the properties for the Master page user interface
    public class MasterPageItem
    {
        public string Title { get; set; }

        public Type TargetType { get; set; }

        public int [,] Difficulty;

       // public Color Color { get; set; }

       //public DelegateCommand<string> Command { get; set; }

       // public string CommandParameter { get; set; }
    }

}
