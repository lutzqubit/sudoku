﻿using System;
using System.IO;
using System.Linq;
using NUnit.Framework;
using Xamarin.UITest;
using Xamarin.UITest.Android;
using Xamarin.UITest.Queries;

namespace AndroidUITest
{
	[TestFixture]
	public class Tests
	{
		AndroidApp app;

		[SetUp]
		public void BeforeEachTest()
		{
			// TODO: If the Android app being tested is included in the solution then open
			// the Unit Tests window, right click Test Apps, select Add App Project
			// and select the app projects that should be tested.
			app = ConfigureApp
				.Android
				// TODO: Update this path to point to your Android app and uncomment the
				// code if the app is not included in the solution.
				.ApkFile("/Users/newuser/Downloads/com.app.qliknsolve.apk")
				.StartApp();
		}

		[Test]
		public void AppLaunches()
		{
			//main menu tap
			app.Screenshot("First screen.");

			app.WaitForElement(x => x.Id("up"),
							   "Failed at home screen",
							   new TimeSpan(0, 0, 30),
							   null,
							   null);
			app.Tap(x => x.Id("up"));

			app.Screenshot("Tapped on view with class: ImageView");


			//Easy board tap

			app.WaitForElement(x => x.Text("Easy Board"),
							   "Failed at Easy board tap",
							   new TimeSpan(0, 0, 30),
							   null,
							   null);
			app.Tap(x => x.Text("Easy Board"));
			app.Screenshot("Tapped on view with class: TextView");

			//Solve tap
			app.WaitForElement(x => x.Text("Solve"),
							   "Failed at Solve tap",
							   new TimeSpan(0, 0, 30),
							   null,
							   null);

			app.Tap(x => x.Text("Solve"));
			app.Screenshot("Tapped on view with class: Button");


			app.WaitForElement(x => x.Id("up"),
							   "Failed while solving",
							   new TimeSpan(0, 0, 100),
							   null,
							   null);

			app.Tap(x => x.Id("up"));

			app.Screenshot("Tapped on view with class: ImageView");

			//Medium Board tap
			app.WaitForElement(x => x.Text("Medium Board"),
							   "Failed at Medium board tap",
							   new TimeSpan(0, 0, 30),
							   null,
							   null);
			app.Tap(x => x.Text("Medium Board"));

			app.Screenshot("Tapped on view with class: TextView");

			//Solve tap
			app.WaitForElement(x => x.Text("Solve"),
							   "Failed at Solve tap",
							   new TimeSpan(0, 0, 30),
							   null,
							   null);

			app.Tap(x => x.Text("Solve"));
			app.Screenshot("Tapped on view with class: Button");


			app.WaitForElement(x => x.Id("up"),
							   "Failed while solving",
							   new TimeSpan(0, 0, 100),
							   null,
							   null);

			app.Tap(x => x.Id("up"));

			app.Screenshot("Tapped on view with class: ImageView");

			//hard board tap
		  app.WaitForElement(x => x.Text("Hard Board"),
							   "Failed at Hard Board tap",
							   new TimeSpan(0, 0, 30),
							   null,
							   null);
			app.Tap(x => x.Text("Hard Board"));

			app.Screenshot("Tapped on view with class: TextView");

			//Solve tap
			app.WaitForElement(x => x.Text("Solve"),
							   "Failed at Solve tap",
							   new TimeSpan(0, 0, 30),
							   null,
							   null);

			app.Tap(x => x.Text("Solve"));
			app.Screenshot("Tapped on view with class: Button");


			app.WaitForElement(x => x.Id("up"),
							   "Failed while solving",
							   new TimeSpan(0, 0, 100),
							   null,
							   null);

			app.Tap(x => x.Id("up"));

			app.Screenshot("Tapped on view with class: ImageView");


			//New board tap
			app.WaitForElement(x => x.Text("New Board"),
								 "Failed at New Board tap",
								 new TimeSpan(0, 0, 30),
								 null,
								 null);
			app.Tap(x => x.Text("New Board"));

			app.Screenshot("Tapped on view with class: TextView");

		//back press

			app.WaitForElement(x => x.Text("Back"),
								 "Failed at Back tap",
								 new TimeSpan(0, 0, 30),
								 null,
								 null);
			app.Tap(x => x.Text("Back"));

			app.Screenshot("Tapped on view with class: Button");
		}

		[Test]
		public void NewTest()
		{
			


		
		}



	}
  }
